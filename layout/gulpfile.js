var gulp = require('gulp');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var less = require('gulp-less');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var env = process.env.GULP_ENV;

/* main page dependencies */
gulp.task('main-js', function () {
    return gulp.src([
        'bower_components/startbootstrap-landing-page-1.0.4/js/jquery.js',
        'bower_components/startbootstrap-landing-page-1.0.4/js/bootstrap.min.js'])
        //.pipe(concat('javascript.js'))
        .pipe(gulpif(env === 'prod', uglify()))
        //.pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('../web/js/main'));
});

gulp.task('main-css', function () {
    return gulp.src([
        'bower_components/startbootstrap-landing-page-1.0.4/css/bootstrap.min.css',
        'bower_components/startbootstrap-landing-page-1.0.4/css/landing-page.css'])
        .pipe(gulpif(env === 'prod', uglifycss()))
        //.pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('../web/css/main'));
});

gulp.task('main-img', function() {
    return gulp.src('bower_components/startbootstrap-landing-page-1.0.4/img/*.*')
        .pipe(gulp.dest('../web/img/main'));
});

/* base project dependencies */
gulp.task('base-js', function () {
    return gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/metisMenu/dist/metisMenu.min.js',
        'bower_components/startbootstrap-sb-admin-2/dist/js/sb-admin-2.js',
        'custom/js/global.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('concat.js'))
        .pipe(gulp.dest('../web/js/base'))
        .pipe(gulpif(env === 'prod', uglify()))
        .pipe(rename('all.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('../web/js/base'));
});

gulp.task('base-css', function () {
    return gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/metisMenu/dist/metisMenu.min.css',
        'bower_components/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css',
        'bower_components/font-awesome/css/font-awesome.min.css'])
        .pipe(sourcemaps.init())
        .pipe(concat('concat.css'))
        .pipe(gulp.dest('../web/css/base'))
        .pipe(gulpif(env === 'prod', uglify()))
        .pipe(rename('all.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('../web/css/base'));
});

gulp.task('base-img', function() {
//    return gulp.src('bower_components/startbootstrap-sb-admin-2/img/*.*')
//        .pipe(gulp.dest('../web/img/base'));
});


gulp.task('default', ['main-js', 'main-css', 'main-img', 'base-js', 'base-css', 'base-img']);