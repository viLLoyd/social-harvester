<?php

namespace AppBundle\Service\Media;

use AppBundle\Entity\User;

/**
 * Service for interacting with Facebook Graph API
 */
class Facebook implements MediaReadInterface
{
    /**
     * @var User Current user instance
     */
    protected $user;

    /**
     * @var string Graph address with current version
     */
    protected $baseUri = 'https://graph.facebook.com/v2.4/';

    /**
     * @param $securityContext
     */
    public function __construct($securityContext)
    {
        $this->user = $securityContext->getToken()->getUser();
    }

    /**
     * {@inheritDoc}
     */
    public function getPosts($limit = 5)
    {
        /*
        $url = $this->baseUri . 'me?fields=' .
            'posts.limit(' . intval($limit) .')' .
            '&access_token=' . $this->user->getFacebookAccessToken();
        */
        $url = $this->baseUri . 'me/feed' .
            '?access_token=' . $this->user->getFacebookAccessToken();

        // setup curl options
        $options = array(
            CURLOPT_URL            => $url,
            CURLOPT_HEADER         => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FRESH_CONNECT  => true
        );

        // perform request
        $cUrl = curl_init( $url );
        curl_setopt_array( $cUrl, $options );
        $response = curl_exec( $cUrl );

        // split into header and body
        $info            = curl_getinfo( $cUrl );
        $responseHeaders = substr( $response, 0, $info['header_size'] );
        $responseBody    = substr( $response, $info['header_size'] );

        return json_decode($responseBody, 1);
    }
}