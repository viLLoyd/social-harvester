<?php

namespace AppBundle\Service\Media;

use AppBundle\Entity\User;

/**
 * Service for interacting with Google+ API
 */
class Google implements MediaReadInterface
{
    /**
     * @var User Current user instance
     */
    protected $user;

    /**
     * @var string Base API address with version included
     */
    protected $baseUri = 'https://www.googleapis.com/plus/v1/';

    public function __construct($securityContext)
    {
        $this->user = $securityContext->getToken()->getUser();
    }

    /**
     * {@inheritDoc}
     */
    public function getPosts($limit = 5)
    {
        $url = $this->baseUri . 'people/me/activities/public';

        // setup curl options
        $options = array(
            CURLOPT_URL            => $url,
            CURLOPT_HEADER         => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FRESH_CONNECT  => true,
            CURLOPT_HTTPHEADER     => array(
                'Authorization: Bearer ' . $this->user->getGoogleAccessToken()
            )
        );

        // perform request
        $cUrl = curl_init( $url );
        curl_setopt_array( $cUrl, $options );
        $response = curl_exec( $cUrl );

        // split into header and body
        $info            = curl_getinfo( $cUrl );
        $responseHeaders = substr( $response, 0, $info['header_size'] );
        $responseBody    = substr( $response, $info['header_size'] );

        return json_decode($responseBody, 1);
    }
}