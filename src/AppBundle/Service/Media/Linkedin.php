<?php

namespace AppBundle\Service\Media;

use AppBundle\Entity\User;

/**
 * Service for interacting with LinkedIn API
 */
class Linkedin implements MediaReadInterface
{
    /**
     * @var User Current user instance
     */
    protected $user;

    /**
     * @var string
     */
    protected $baseUri = '';

    /**
     * @param $securityContext
     */
    public function __construct($securityContext)
    {
        $this->user = $securityContext->getToken()->getUser();
    }

    /**
     * {@inheritDoc}
     */
    public function getPosts($limit = 5)
    {

    }
}