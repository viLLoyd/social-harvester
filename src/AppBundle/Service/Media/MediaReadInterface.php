<?php

namespace AppBundle\Service\Media;

/**
 * Interface defining social media fetch methods
 */
interface MediaReadInterface
{
    /**
     * Fetch user posts
     *
     * @param int $limit  Number of posts
     *
     * @return array
     */
    public function getPosts($limit = 5);
}