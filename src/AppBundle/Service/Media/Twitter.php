<?php

namespace AppBundle\Service\Media;

use AppBundle\Entity\User;

/**
 * Service for interacting with Facebook Graph API
 */
class Twitter implements MediaReadInterface
{
    /**
     * @var User Current user instance
     */
    protected $user;

    /**
     * @var string Twitter app key
     */
    protected $appKey;

    /**
     * @var string Twitter app secret
     */
    protected $appSecret;

    /**
     * @var string Base API address with current version
     */
    protected $baseUri = 'https://api.twitter.com/1.1/';

    /**
     * @param $securityContext
     * @param $appKey Twitter app key
     * @param $appSecret Twitter app secret
     */
    public function __construct($securityContext, $appKey, $appSecret)
    {
        $this->user      = $securityContext->getToken()->getUser();
        $this->appKey    = $appKey;
        $this->appSecret = $appSecret;
    }

    /**
     * {@inheritDoc}
     */
    public function getPosts($limit = 5)
    {
        $url   = $this->baseUri . 'statuses/home_timeline.json';
        $now   = time();
        $nonce = md5(microtime(true).uniqid('', true));

        // setup curl options
        $options = array(
            CURLOPT_URL            => $url,
            CURLOPT_HEADER         => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FRESH_CONNECT  => true,
            CURLOPT_HTTPHEADER     => array(
                'Authorization:
                        OAuth oauth_consumer_key="' . $this->appKey . '",
                              oauth_nonce="' . $nonce . '",
                              oauth_signature="' . $this->generateSignature($now, $nonce) . '",
                              oauth_signature_method="HMAC-SHA1",
                              oauth_timestamp="' . $now . '",
                              oauth_token="' . $this->user->getTwitterAccessToken() . '",
                              oauth_version="1.0"'
            )
        );

        // perform request
        $cUrl = curl_init( $url );
        curl_setopt_array( $cUrl, $options );
        $response = curl_exec( $cUrl );

        // split into header and body
        $info            = curl_getinfo( $cUrl );
        $responseHeaders = substr( $response, 0, $info['header_size'] );
        $responseBody    = substr( $response, $info['header_size'] );

        return json_decode($responseBody, 1);
    }

    /**
     * Generate Twitter specific signature for authenticated request
     *
     * @param $now integer Current timestamp
     * @param $nonce string Unique string
     * @return string Generated signature
     */
    private function generateSignature($now, $nonce)
    {
        $signatureBaseStr = 'GET&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fhome_timeline.json&' .
            'oauth_consumer_key%3D' . $this->appKey .
            '%26oauth_nonce%3D' . $nonce .
            '%26oauth_signature_method%3DHMAC-SHA1' .
            '%26oauth_timestamp%3D' . $now .
            '%26oauth_token%3D' . $this->user->getTwitterAccessToken() .
            '%26oauth_version%3D1.0';

        $combinedSecrets  = $this->appSecret . '&' . $this->user->getTwitterAccessTokenSecret();

        return urlencode(base64_encode(hash_hmac('sha1', $signatureBaseStr, $combinedSecrets, true)));
    }
}