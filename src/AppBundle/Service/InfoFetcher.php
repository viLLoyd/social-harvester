<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Service\Media\Facebook;
use AppBundle\Service\Media\Google;
use AppBundle\Service\Media\Linkedin;
use AppBundle\Service\Media\Twitter;

/**
 * Service for fetching user posts from different media providers
 */
class InfoFetcher
{
    /** @var Facebook Facebook service */
    protected $facebook;

    /** @var Google Google+ Service*/
    protected $google;

    /** @var Linkedin Linkedin Service */
    protected $linkedin;

    /** @var Twitter Twitter Service */
    protected $twitter;

    protected $allowedServices = array(
        'facebook',
        'google',
        'linkedin',
        'twitter'
    );

    public function __construct($facebookService, $googleService, $linkedInService, $twitterService)
    {
        $this->facebook = $facebookService;
        $this->google   = $googleService;
        $this->linkedin = $linkedInService;
        $this->twitter  = $twitterService;
    }

    /**
     * Get user posts by service
     *
     * @param string $serviceName Name of service
     * @throws \Exception
     *
     * @return array
     */
    public function getPostsByService($serviceName)
    {
        if (!in_array($serviceName, $this->allowedServices)) {
            throw new \Exception("Service {$serviceName} is not supported");
        }

        $posts = $this->$serviceName->getPosts();

        return $posts;
    }

    /**
     * Get all posts from user's connected services
     *
     * @param User $user Current user instance
     * @return array
     */
    public function getUserPosts(User $user)
    {
        $posts = array();

        foreach($user->getSupportedServices() as $serviceName) {
            $posts = array_merge($posts, $this->$serviceName->getPosts());
        }

        return $posts;
    }
}