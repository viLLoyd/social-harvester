<?php
namespace AppBundle\Service;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\EntityUserProvider as HWIUserInterface;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use AppBundle\Entity\User;

class OAuthUserProvider extends HWIUserInterface
{

    protected $passwordEncoder;

    /**
     * Constructor.
     *
     * @param ManagerRegistry $registry    Manager registry.
     * @param string          $class       User entity class to load.
     * @param array           $properties  Mapping of resource owners to properties
     * @param mixed           $passwordEncoder  Password encoder service
     * @param string          $managerName Optional name of the entity manager to use
     */
    public function __construct(ManagerRegistry $registry, $class, array $properties, $passwordEncoder, $managerName = null)
    {
        parent::__construct($registry, $class, $properties, $managerName);

        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property    = $this->getProperty($response);
        $userId      = $response->getUsername();
        $serviceName = $response->getResourceOwner()->getName();

        //on connect - get the access token and the user ID
        $service     = $response->getResourceOwner()->getName();
        $setter      = 'set'.ucfirst($service);
        $setterId    = $setter.'Id';
        $setterToken = $setter.'AccessToken';

        //"disconnect" previously connected users
        if (null !== $previousUser = $this->em->getRepository('AppBundle:User')->findOneBy(array($property => $userId))) {
            $previousUser->$setterId(null);
            $previousUser->$setterToken(null);
        }

        //connect current user
        $user->$setterId($userId);
        $user->$setterToken($response->getAccessToken());
        if ($serviceName == 'twitter') {
            $user->setTwitterAccessTokenSecret($response->getTokenSecret());
        }

        $this->em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $userId      = $response->getUsername();
        $userDetails = $response->getResponse();
        $username    = $response->getEmail() ? $response->getEmail() : $userId;
        $serviceName = $response->getResourceOwner()->getName();

        /** @var User $user */
        $user = $this->repository->findOneBy(array($this->getProperty($response) => $userId));

        // case when registering
        if (null === $user) {
            //check if email(username) already exists
            if (count($this->repository->findOneBy(array('username' => $username)))) {
                throw new AuthenticationException(
                    'User matching this id already exists',
                    100
                );
            }

            $service     = $response->getResourceOwner()->getName();
            $setter      = 'set'.ucfirst($service);
            $setterId    = $setter.'Id';
            $setterToken = $setter.'AccessToken';

            // create new user based on response data
            $user = new User();
            $user->$setterId($userId);
            $user->$setterToken($response->getAccessToken());
            if ($serviceName == 'twitter') {
                $user->setTwitterAccessTokenSecret($response->getTokenSecret());
            }

            // populate user data
            $user->setUsername($username);
            $user->setEmail($response->getEmail());
            $user->setPassword($username);
            $user->setRoles(array('ROLE_USER'));
            $user->setIsActive(true);
            $user->setCreatedAt(new \DateTime());
            $user->setLastLoginDate(new \DateTime());

            $fullName = explode(' ', $response->getRealName());

            if (count($fullName) > 1) {
                $user->setFirstName($fullName[0]);
                $user->setLastName($fullName[1]);
            }

            $encodedPass = $this->passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encodedPass);

            $this->em->persist($user);
            $this->em->flush();

            return $user;
        }

        //if user exists - go with the HWIOAuth way
        $user        = parent::loadUserByOAuthUserResponse($response);
        $setter      = 'set' . ucfirst($serviceName) . 'AccessToken';

        //update access token
        $user->$setter($response->getAccessToken());
        if ($serviceName == 'twitter') {
            $user->setTwitterAccessTokenSecret($response->getTokenSecret());
        }

        return $user;
    }

    /**
     * Gets the property for the response.
     *
     * @param UserResponseInterface $response
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected function getProperty(UserResponseInterface $response)
    {
        $resourceOwnerName = $response->getResourceOwner()->getName();

        if (!isset($this->properties[$resourceOwnerName])) {
            throw new \RuntimeException(sprintf("No property defined for entity for resource owner '%s'.", $resourceOwnerName));
        }

        return $this->properties[$resourceOwnerName];
    }

}