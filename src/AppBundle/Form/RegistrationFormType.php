<?php
namespace AppBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'email', array(
                'label'       => 'user.email',
                'required'    => true,
                'constraints' => array(
                    new Constraints\NotBlank(),
                    new Constraints\Length(array('max' => 100))
                ),
                'attr'        => array(
                    'class' => 'form-control'
                )
            ))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'first_options' => array(
                    'label'       => 'user.password',
                    'constraints' => array(
                        new Constraints\NotBlank(),
                        new Constraints\Length(array('max' => 100))
                    ),
                    'attr'        => array('class' => 'form-control')
                ),
                'second_options' => array(
                    'label'       => 'user.password_confirmation',
                    'constraints' => array(
                        new Constraints\NotBlank(),
                        new Constraints\Length(array('max' => 100))
                    ),
                    'attr'        => array('class' => 'form-control')
                ),
                'invalid_message' => 'user.password_mismatch',
            ))
            ->add( 'firstName', 'text', array(
                'label'       => 'user.first_name',
                'required'    => true,
                'attr'        => array(
                    'class' => 'form-control'
                ),
                'constraints' => array(
                    new Constraints\NotBlank(),
                    new Constraints\Length(array('max' => 100))
                )
            ))
            ->add( 'lastName', 'text', array(
                'label'       => 'user.last_name',
                'required'    => true,
                'attr'        => array(
                    'class' => 'form-control'
                ),
                'constraints' => array(
                    new Constraints\NotBlank(),
                    new Constraints\Length(array('max' => 100))
                )
            ))
            ->add('country', 'country', array(
                'label'       => 'user.country',
                'required'    => true,
                'placeholder' => 'user.country_select',
                'constraints' => array(
                    new Constraints\NotBlank()
                ),
                'attr'     => array(
                    'class' => 'form-control'
                )
            ));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
            'data_class' => 'AppBundle\Entity\User'
        ) );
    }

    public function getName()
    {
        return 'registration';
    }

}
