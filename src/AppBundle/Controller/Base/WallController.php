<?php

namespace AppBundle\Controller\Base;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class WallController extends Controller
{
    /**
     * @Route("/wall", name="user_wall")
     */
    public function indexAction(Request $request)
    {
        $feedService = $this->get('info_fetcher');

        //$feedService->getPostsByService('twitter');


        return $this->render('base/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }
}
