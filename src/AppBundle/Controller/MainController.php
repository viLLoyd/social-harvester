<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\RegistrationFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MainController extends Controller
{
    /**
     * @Route("/", name="welcome_page")
     */
    public function indexAction(Request $request)
    {
        if ($this->getUser()) {
            return $this->redirect($this->generateUrl('user_wall'));
        }
        // replace this example code with whatever you need
        return $this->render('welcome.html.twig', array());
    }

    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        if ($this->getUser()) {
            return $this->redirect($this->generateUrl('user_wall'));
        }

        $authenticationUtils = $this->get('security.authentication_utils');
        $error               = $authenticationUtils->getLastAuthenticationError();
        $lastUsername        = $authenticationUtils->getLastUsername();

        return $this->render('main/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
        // this controller will not be executed,
        // as the route is handled by the Security system
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
        // The security layer will intercept this request
    }

    /**
     *
     * @Route("/logout-user", name="logout_back")
     *
     * @param Request $request
     *
     * @return array
     */
    public function logoutUserAction( Request $request )
    {
        $allowedToSwitch = $this->get( 'security.context' )->isGranted( 'ROLE_PREVIOUS_ADMIN' );

        if ( $allowedToSwitch ) {
            return $this->redirect( $request->getBaseUrl() . '/?_make_me=_exit' );
        } else {
            return $this->redirect( $this->generateUrl( 'logout' ) );
        }
    }

    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request)
    {
        $em   = $this->getDoctrine()->getManager();
        $user = new User();
        $form = $this->createForm( new RegistrationFormType(), $user, array(
            'method' => 'POST',
            'action' => $this->generateUrl('register')
        ) );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($this->populateUser($form->getData()));
            $em->flush();

            return $this->redirect($this->generateUrl('user_wall'));
        }

        return $this->render('main/register.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Create new user based on submitted data
     *
     * @param $user User populated user instance from request
     * @return User
     */
    private function populateUser(User $user)
    {
        $user->setEmail($user->getUsername());
        $user->setRoles(array('ROLE_USER'));
        $user->setIsActive(true);
        $user->setCreatedAt(new \DateTime());
        $user->setLastLoginDate(new \DateTime());

        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($user, $user->getPassword());

        $user->setPassword($encoded);

        return $user;
    }
}
